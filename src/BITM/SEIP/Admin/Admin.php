<?php

namespace App\BITM\SEIP\Admin;

use App\BITM\SEIP\Message\Message;
use App\BITM\SEIP\Utility\Utility;

use App\BITM\SEIP\Model\Database as DB;
use PDO;
use PDOException;

class Admin extends DB
{
    private $transaction_id;
    private $Uid;
    private $amount;
    private $source;



    public function setData($postData){

        if(array_key_exists('transaction_id',$postData)){
            $this->transaction_id = $postData['transaction_id'];
        }

        if(array_key_exists('amount',$postData)){
            $this->amount = $postData['amount'];
        }

        if(array_key_exists('Uid',$postData)){
            $this->Uid = $postData['Uid'];
        }

        if(array_key_exists('source',$postData)){
            $this->source = $postData['source'];
        }



    }




    public function store(){

        //Utility::dd($_POST);
        $arrData = array($this->transaction_id,$this->amount,$this->Uid,$this->source);



        $sql = "INSERT into income(transaction_id,amount,Uid,source) VALUES(?,?,?,?)";



        $STH = $this->conn->prepare($sql);


        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");


       // $_SESSION=$_POST;

        Utility::redirect('add_income.php');


    }


    public function storeCircle(){

        $arrData = array($this->circle_no,$this->ward_no,$this->percentage);

        $sql = "INSERT into circle(circle_no,ward_no,percentage) VALUES(?,?,?)";

        $STH = $this->conn->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('../Admin/index.php');


    }

    public function index(){

        $sql = "select * from income";

        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function storeUser(){

        $arrData = array($this->building_id,$this->first_name,$this->last_name,$this->father_name,$this->mother_name,$this->birth_date,$this->gender,$this->mobile_no,$this->email,$this->address,$this->NID,$this->holder_image);

        $sql = "INSERT into holder(building_id,first_name,last_name,father_name,mother_name,birth_date,gender,mobile_no,email,address,nid,holder_image) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";

        $STH = $this->conn->prepare($sql);

        $result = $STH->execute($arrData);

        //$_SESSION=$_POST;

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        //Utility::dd($_SESSION);
        Utility::redirect('../User/Profile/signup.php');


    }

    public function indexCircle()
    {

        $sql = "select * from circle";
        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();
    }




    public function indexWard()
    {

        $sql = "select * from circle WHERE ";

        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

    public function indexTax($holding_no)
    {
        $sql = "select * from tax_info WHERE holding_no=".$holding_no;

        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();
    }


    public function indexPaginator($page=1,$itemsPerPage=3)
    {
        try
        {
            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from building INNER JOIN holder ON building.id=holder.building_id LIMIT $start,$itemsPerPage";
        }
        catch (PDOException $error)
        {

            $sql = "select * from building INNER JOIN holder ON building.id=holder.building_id";

        }


        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

        $arrSomeData  = $STH->fetchAll();

        return $arrSomeData;


    }



    public function view(){


        $sql = 'SELECT * FROM building,holder WHERE building.id=holder.building_id AND building.id='.$this->id;

        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }

    public function Uview(){

        $sql = 'SELECT * FROM building,holder WHERE building.id=holder.building_id AND building.id='.$this->building_id;

       // $sql = "select * from building as b inner join holder as w on b.holding_no=w.holding_no where b.holding_no='$holding_no'";
        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $result=$STH->fetch();

        return $result;


    }


    public function viewTax(){

        $sql = 'SELECT * FROM tax_info WHERE tax_info.holding_no='.$this->holding_no;

        // $sql = "select * from building as b inner join holder as w on b.holding_no=w.holding_no where b.holding_no='$holding_no'";
        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $result=$STH->fetch();

        return $result;


    }




    public function updateBuilding(){

        $arrData = array($this->reg_date,$this->reg_no,$this->b_name,$this->area_name,$this->b_type,$this->tenant_type,$this->block,$this->circle,$this->ward,$this->b_area,$this->holding_no);

        $sql = "UPDATE  building SET reg_date=?,reg_no=?,b_name=?,area_name=?,b_type=?,tenant_type=?,block=?,circle=?,ward=?,b_area=?,holding_no=? WHERE id=".$this->id;


        $STH = $this->conn->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");


        //Utility::dd($this->id);
        $_SESSION['id']=$this->id;

        Utility::redirect('editUser.php');


    }


    public function updatetaxInfo(){

        $arrData = array($this->amount,$this->due_amount);

        $sql = "UPDATE  tax_info SET amount=?,due_amount=? WHERE holding_no=".$this->holding_no;


        $STH = $this->conn->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");


        Utility::redirect('index.php');


    }



    public function updateBuildingFiles(){

        $arrData = array($this->b_image,$this->b_doc_image);

        $sql = "UPDATE  building SET b_image=?,b_doc_image=? WHERE id=".$this->id;


        $STH = $this->conn->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");

        //var_dump($result);

        Utility::redirect('editBuilding.php');


    }




    public function updateUser(){

        $arrData = array($this->first_name,$this->last_name,$this->father_name,$this->mother_name,$this->birth_date,$this->gender,$this->mobile_no,$this->email,$this->address,$this->NID);

        $sql = "UPDATE  holder SET first_name=?,last_name=?,father_name=?,mother_name=?,birth_date=?,gender=?,mobile_no=?,email=?,address=?,nid=? WHERE building_id=".$this->building_id;


        $STH = $this->conn->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");

        //var_dump($result);

        Utility::redirect('holderlist.php');


    }

    public function updateUserLogDate($date){
        //Utility::dd($this->holding_no);

        $arrData = array($date);

        $sql = "UPDATE  holder SET login_date=? WHERE holding_no=".$this->holding_no;


        $STH = $this->conn->prepare($sql);

        $STH->execute($arrData);
    }


    public function updateUserPicture(){

        $arrData = array($this->holder_image);

        $sql = "UPDATE  holder SET holder_image=? WHERE building_id=".$this->building_id;


        $STH = $this->conn->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");

        var_dump($result);
        Utility::redirect('holderlist.php');


    }



    public function userView(){

        $sql = 'SELECT * FROM building WHERE building.holding_no='.$this->holding_no;

       // Utility::dd($sql);
        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);


        $result=$STH->fetch();

        return $result;
    }


    public function getBuildingId(){


        $sql = 'SELECT id FROM building ORDER BY id DESC LIMIT 1';

        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();
    }


    public function delete(){

        $sql = "delete from building  WHERE id=".$this->id;

        $result = $this->conn->exec($sql);



        if($result)
            Message::message("Success! Data Has Been Permanently Deleted :)");
        else
            Message::message("Failed! Data Has Not Been Permanently Deleted  :( ");


        Utility::redirect('holderlist.php');


    }


    public function deleteMultiple($selectedIDsArray){


        foreach($selectedIDsArray as $id) {

            $sql = "delete from building  WHERE id=" . $id;

            $result = $this->conn->exec($sql);

            if (!$result) break;


            elseif ($result)
                Message::message("Success! All Seleted Data Has Been  Deleted Successfully :)");
            else
                Message::message("Failed! All Selected Data Has Not Been Deleted  :( ");

            Utility::redirect('holderlist.php?Page=1');
        }

    }



    public function listSelectedData($selectedIDs){



        foreach($selectedIDs as $id){

            $sql = "Select * from building,holder  WHERE building.id=holder.building_id AND building.id=".$id;


            $STH = $this->conn->query($sql);

            $STH->setFetchMode(PDO::FETCH_OBJ);

            $someData[]  = $STH->fetch();


        }


        return $someData;


    }



}