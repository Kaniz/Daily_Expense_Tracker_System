<?php
namespace App\BITM\SEIP\Admin;
use App\BITM\SEIP\Message\Message;
use App\BITM\SEIP\Utility\Utility;
use App\BITM\SEIP\Model\Database as DB;
use PDO;


class User extends DB{
    private $full_name;
    private $email;
    public $password;
    private $gender;
    private $contact;
    private $Address;
    private $Uid;

    public function __construct(){
        parent::__construct();
    }

    public function setData($postData=array()){
        if(array_key_exists('full_name',$postData)){
            $this->full_name = $postData['full_name'];
        }

        if(array_key_exists('email',$postData)){
            $this->email = $postData['email'];
        }

        if(array_key_exists('password',$postData)){
            $this->password = $postData['password'];
        }

        if(array_key_exists('contact',$postData)){
            $this->contact = $postData['contact'];
        }
        if(array_key_exists('Address',$postData)){
            $this->Address = $postData['Address'];
        }

        if(array_key_exists('Uid',$postData)){
            $this->Uid = $postData['Uid'];
        }

        if(array_key_exists('gender',$postData)){
            $this->gender = $postData['gender'];
        }



        return $this;
    }




    public function store() {

        //Utility::dd($_POST);
        $arrData = array($this->full_name,$this->email,$this->password,$this->contact,$this->Address,$this->Uid,$this->gender);


        $sql = "INSERT into users(full_name,email,password,contact,Address,Uid,gender) VALUES(?,?,?,?,?,?,?)";



        $STH = $this->conn->prepare($sql);


        $result =$STH->execute($arrData);


        // $_SESSION=$_POST;
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");

            return Utility::redirect("signup.php");
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been stored successfully.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        }
    }



   /* public function view(){
        $query=" SELECT * FROM admin WHERE email = '$this->email' ";
       // Utility::dd($query);
        $STH =$this->conn->query($query);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }// end of view()

    
  */

    public function index()
    {

        $sql = "select * from users";

        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();
    }


}

