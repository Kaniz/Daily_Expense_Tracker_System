-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 15, 2017 at 04:27 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prototype_tracker_system`
--
CREATE DATABASE IF NOT EXISTS `prototype_tracker_system` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `prototype_tracker_system`;

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `expenses_id` int(111) NOT NULL,
  `user_id` varchar(111) DEFAULT NULL,
  `amount` float NOT NULL,
  `source` varchar(111) NOT NULL,
  `date` varchar(111) NOT NULL,
  `soft_deleted` varchar(111) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`expenses_id`, `user_id`, `amount`, `source`, `date`, `soft_deleted`) VALUES
(1, NULL, 2555, '', '', 'No'),
(2, NULL, 1500, '', '', 'No'),
(3, NULL, 1500, '', '', 'No'),
(4, NULL, 2888, '', '', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

CREATE TABLE `income` (
  `id` int(15) NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `Uid` int(15) NOT NULL,
  `amount` int(255) NOT NULL,
  `source` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `income`
--

INSERT INTO `income` (`id`, `transaction_id`, `Uid`, `amount`, `source`) VALUES
(1, '45646', 45646, 0, 'salary'),
(2, '123', 456, 789, 'salary'),
(3, '456', 789, 456, 'salary'),
(4, '564564645', 4656546, 4564545, 'asfafsasf');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `gender` varchar(15) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `Address` varchar(20) NOT NULL,
  `Uid` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `email`, `password`, `gender`, `contact`, `Address`, `Uid`) VALUES
(2, 'Shamim', 'shamim10cse@gmail.com', '123456', 'Male', '01846028856', 'Ctg', 10224),
(3, 'shakil', 'shakil@com', '123456', 'Male', '00555', 'DHaka', 10666),
(4, 'ass', 'shakilf55@gmail.com', '456564', 'Mla', '1351564', 'afsfas', 102);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`expenses_id`);

--
-- Indexes for table `income`
--
ALTER TABLE `income`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `expenses_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `income`
--
ALTER TABLE `income`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
