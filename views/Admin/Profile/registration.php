<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP\Admin\User;
use App\BITM\SEIP\Admin\Auth;
use App\BITM\SEIP\Message\Message;
use App\BITM\SEIP\Utility\Utility;
$auth= new Auth();

//Utility::dd($_POST);
$status= $auth->setData($_POST)->is_exist();
if($status){
    Message::setMessage("<div class='alert alert-danger'>
    <strong>Taken!</strong> Email has already been taken. </div>");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}else{

    $obj= new User();

    $obj->setData($_POST)->store();

}
